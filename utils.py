import pandas as pd
import numpy as np
import seaborn as sb
import matplotlib.pyplot as plt
import scipy.cluster.hierarchy as hclust

def nan_replace(t):
    nume_variabile = list(t.columns)
    for each in nume_variabile:
        if any(t[each].isna()):
            if(pd.api.types.is_numeric_dtype(t[each])):
                t[each].fillna(t[each].mean(), inplace = True)
            else:
                t[each].fillna(t[each].mode()[0], inplace= True)

def tabelare_matrice(x, nume_linii=None, nume_coloane=None, nume_fisier = "out.csv"):
    t = pd.DataFrame(x,nume_linii, nume_coloane)
    t.to_csv(nume_fisier)
    return t

def tabelare_varianta(alpha, etichete):
    procente = alpha * 100 / sum(alpha)
    tabelare_varianta = pd.DataFrame(data={
        "Varianta": alpha,
        "Varianta cumulata": np.cumsum(alpha),
        "Procent Varianta": procente,
        "Procent cumulat": np.cumsum(procente)
    }, index=etichete)
    return tabelare_varianta

def corelograma(x, val_min=-1, val_max = 1, titlu ="Corelatii factoriale"):
    fig = plt.figure(titlu, figsize=(9,9))
    ax = fig.add_subplot(1,1,1)
    ax_ = sb.heatmap(data=x, vmin=val_min, vmax=val_max, cmap="RdYlBu", annot=True, ax=ax)
    ax_.set_xticklabels(x.columns, ha="right", rotation=30) # cred ca le voi lua ca atare, nu inteleg, e too much

def plot_componente(x, var_x, var_y, titlu = "Plot componente"):
    fig = plt.figure(titlu, figsize=(9,9))
    ax = fig.add_subplot(1,1,1)
    ax.set_xlabel(var_x, fontdict={"fontsize": 12, "color":"b"})
    ax.set_ylabel(var_y, fontdict={"fontsize": 12, "color":"b"})

    ax.scatter(x[var_x], x[var_y], color="r")

    for i in range(len(x)):
        ax.text(x[var_x].iloc[i], x[var_y].iloc[i], x.index[i])

def partitie(h, nr_clusteri, p, instante):
    k_dif_max = p-nr_clusteri
    prag = (h[k_dif_max, 2] + h[k_dif_max+1,2])/2

    fig = plt.figure(figsize=(9,9))
    ax = fig.add_subplot(1,1,1)
    ax.set_title("Partitionare cu " + str(nr_clusteri) + " clusteri - metoda Ward")
    hclust.dendrogram(h, labels=instante, ax=ax, color_threshold=prag)

    n = p + 1

    c=np.arange(n)
    for i in range(n - nr_clusteri):
        k1 = h[i, 0]
        k2 = h[i, 1]

        c[c==k1] = n+i
        c[c==k2] = n+i

    coduri = pd.Categorical(c).codes
    return np.array(["c" + str(cod+1) for cod in coduri])


def histograma(x, variabila, partitia):
    fig = plt.figure(figsize=(9, 9))
    fig.suptitle(" Histograme pt variabila " + variabila)

    clusteri = list(set(partitia))
    dim = len(clusteri)

    axs = fig.subplots(1, dim, sharey=True)

    for i in range(dim):
        ax = axs[i]
        ax.set_xlabel(clusteri[i])
        ax.hist(x[partitia == clusteri[i]], bins=10,
                rwidth=0.9, range=(min(x), max(x)))


def show():
    plt.show()