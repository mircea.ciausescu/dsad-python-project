import numpy as np
import pandas as pd
import factor_analyzer as fact
from utils import *
import scipy.cluster.hierarchy as hclust

def execute():
    tabel = pd.read_csv("date.csv", index_col=0)

    nan_replace(tabel)

    nume_variabile = list(tabel)[1:]
    print(nume_variabile)
    instante = list(tabel.index)

    x = tabel[nume_variabile].values

    n, m = x.shape

    test_barlett = fact.calculate_bartlett_sphericity(x)
    print("Test Barlett", test_barlett)

    if test_barlett[1] > 0.001:
        print("Nu exista factori comuni")
        exit(0)

    kmo = fact.calculate_kmo(x)
    print("KMO:", kmo)

    tabel_kmo = pd.DataFrame(data={"Index KMO": np.append(kmo[0], kmo[1])},
                             index=nume_variabile + ["Total"])

    print("TABEL KMO", tabel_kmo)

    corelograma(tabel_kmo, val_min=0, val_max=1, titlu="Index KMO")

    if( all(tabel_kmo["Index KMO"] < 0.6) ):
        print("Nu exista factori comuni")
        exit(0)

    rotatie = None
    model_fact = fact.FactorAnalyzer(n_factors=n, rotation=rotatie)
    model_fact.fit(x)
    print("Model factorial: ", model_fact)

    eigenvalues, variance = model_fact.get_eigenvalues()

    variance_table = pd.DataFrame({'Factor': range(1, len(eigenvalues) + 1),
                                   'Eigenvalue': eigenvalues,
                                   'Variance': variance})
    print(variance_table)
    alpha = model_fact.get_factor_variance()[0]
    print("ALPHA", alpha)
    etichete_factori = ["F" + str(i+1) for i in range(m)]
    tabel_varianta = tabelare_varianta(alpha, etichete_factori)
    tabel_varianta.to_csv("Varianta_Factorilor.csv")

    loadings = model_fact.loadings_
    tabel_coeficienti = tabelare_matrice(loadings, nume_variabile, etichete_factori, "Coeficienti.csv")
    corelograma(tabel_coeficienti)

    scoruri = model_fact.transform(x)
    tabel_scoruri = tabelare_matrice(scoruri, tabel.index, etichete_factori, "Scoruri_factoriale.csv")

    comunalitati = model_fact.get_communalities()
    tabel_comunalitati = pd.DataFrame(data={"Comunalitati":comunalitati}, index=nume_variabile)

    corelograma(tabel_comunalitati, titlu="Comunalitati")

    h = hclust.linkage(x, method='ward')
    print('h', h)

    p = n - 1
    k_dif_max = np.argmax(h[1:, 2] - h[:(p-1), 2])
    print("DIFERENTA MAXIMA:", k_dif_max)

    nr_clusteri = p - k_dif_max
    print("Nr clusteri recomandati: ", nr_clusteri)

    partitie_opt = partitie(h, nr_clusteri, p, instante)

    partitie_opt_t = pd.DataFrame(data={"Cluster": partitie_opt}, index= instante)

    partitie_opt_t.to_csv("PartitieOptima.csv")

    partitie_2 = partitie(h, 2, p, instante)
    print("Partitie 2", partitie_2)

    partitie_2_t = pd.DataFrame(data={"Cluster": partitie_2},
                                index=instante)
    partitie_2_t.to_csv("Partitie2.csv")

    partitie_3 = partitie(h, 3, p, instante)
    print("Partitie 3", partitie_2)

    partitie_3_t = pd.DataFrame(data={"Cluster": partitie_3},
                                index=instante)
    partitie_3_t.to_csv("Partitie3.csv")

    for i in range(3):
        histograma(x[:, i], nume_variabile[i], partitie_opt)
    show()

if __name__ == "__main__":
    execute()